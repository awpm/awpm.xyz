---
name: Stack Spec
---
# Stack Specification
There is a tool for every problem

Generalizing stacks is not that easy. But there are few Dos and Don'ts worth to be made part of the AWPM Spec

## Go modular
You should go modular and focus at one problem at the time. Decide how much control you need over a certain area of expertise and split areas zup in easily consumable pieces of responsibily.

## Look at what Google is doing
Always watch out for what Google is doing. This is no joke. Google uses TypeScript, TensorFow, Polymer, Webcomponents, Angular, Docker and much more. You can learn a lot from adopting those technologies and rest assured that they will be supported for a rather long time.
